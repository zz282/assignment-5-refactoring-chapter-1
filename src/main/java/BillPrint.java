import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * BillPrint - adapted from Refactoring, 2nd Edition by Martin Fowler
 *
 * @author Minglun Zhang
 *
 */
public class BillPrint {
	private HashMap<String, Play> plays;
	private String customer;
	private ArrayList<Performance> performances;

	public BillPrint(ArrayList<Play> plays, String customer, ArrayList<Performance> performances) {
		if (plays != null) {
			this.plays = new HashMap();
			for (Play p : plays) {
				this.plays.put(p.getId(), p);}
		}
		this.customer = customer;
		this.performances = performances;
	}

	public HashMap<String, Play> getPlays() {
		return plays;
	}


	public String getCustomer() {
		return customer;
	}

	public ArrayList<Performance> getPerformances() {
		return performances;
	}

	public int thisAmount(Performance aPerformance){
		if (playFor(aPerformance) == null ){
			throw new IllegalArgumentException("No Play Founded");
		}
		int result = 0;
		switch (aPerformance.play.getType()) {
			case "tragedy": result = 40000;
				if (aPerformance.getAudience() > 30) {
					result += 1000 * (aPerformance.getAudience() - 30);
				}
				break;
			case "comedy":  result = 30000;
				if (aPerformance.getAudience() > 20) {
					result += 10000 + 500 * (aPerformance.getAudience() - 20);
				}
				result += 300 * aPerformance.getAudience();
				break;
			default:        throw new IllegalArgumentException("unknown type: " +  playFor(aPerformance).getType());
		}
		return result;
	}
	public Play playFor(Performance aPerformance){
		return plays.get(aPerformance.getPlayID());
	}
	public String usd(int aNumber){
		DecimalFormat numberFormat = new DecimalFormat("#.00");
		return "$"+numberFormat.format((double) aNumber / 100.00);
	}

	public int totalAmount(BillPrint data){
		int result=0;
		for(Performance perf: data.performances){
			result += thisAmount(perf);
		}
		return result;
	}

	public int totalVolumeCredits(BillPrint data){
		int result = 0;
		for(Performance perf: data.performances){
			result += volumeCreditsFor(perf);
		}
		return result;
	}

	public String statement() {
		ArrayList<Performance> temp = new ArrayList<Performance>();
		for (Performance pref: performances){
			temp.add(enrichPerformance(pref));
		}
		BillPrint statementData = new BillPrint(null, customer, temp);
		statementData.plays = plays;
		return renderPlainText(statementData);

	}

	public Performance enrichPerformance(Performance aPerformance){
		Performance result = new Performance(aPerformance.getPlayID(), aPerformance.getAudience());
		result.play = playFor(aPerformance);
		return result;
	}
	public String renderPlainText(BillPrint data) {
		String result = "Statement for " + data.customer + "\n";
		for (Performance perf: data.performances) {
			if (playFor(perf) == null) {
				throw new IllegalArgumentException("No play found");
			}

			result += "  " + perf.play.getName() + ": " + usd(thisAmount(perf)) + " (" + perf.getAudience()
					+ " seats)" + "\n";

		}
		result += "Amount owed is " + usd( totalAmount(data) ) + "\n";
		result += "You earned " + totalVolumeCredits(data) + " credits" + "\n";
		return result;
	}
	public int volumeCreditsFor(Performance aPerformance){
		int result = 0;
		result += Math.max(aPerformance.getAudience() - 30, 0);
		if ( playFor(aPerformance).getType().equals("comedy")) {
			result += Math.floor((double) aPerformance.getAudience() / 5.0);
		}
		return result;
	}

	public static void main(String[] args) {
		Play p1 = new Play("hamlet", "Hamlet", "tragedy");
		Play p2 = new Play("as-like", "As You Like It", "comedy");
		Play p3 = new Play("othello", "Othello", "tragedy");
		ArrayList<Play> pList = new ArrayList<Play>();
		pList.add(p1);
		pList.add(p2);
		pList.add(p3);
		Performance per1 = new Performance("hamlet", 55);
		Performance per2 = new Performance("as-like", 35);
		Performance per3 = new Performance("othello", 40);
		ArrayList<Performance> perList = new ArrayList<Performance>();
		perList.add(per1);
		perList.add(per2);
		perList.add(per3);
		String customer = "BigCo";
		BillPrint app = new BillPrint(pList, customer, perList);
		System.out.println(app.statement());
	}

}
